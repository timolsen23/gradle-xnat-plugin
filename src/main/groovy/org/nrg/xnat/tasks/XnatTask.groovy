package org.nrg.xnat.tasks

import java.nio.file.Files
import java.nio.file.StandardCopyOption

import org.apache.commons.io.FileUtils;
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction
import org.nrg.xft.generators.GenerateResources

class XnatTask extends DefaultTask {
    @TaskAction
    def process() {		
		//where schema are located
		def source = new File(project.projectDir.absolutePath, "src/main/resources")
		
		if(!(new File(source,"schemas")).exists()){
			System.out.println("No schemas found to generate resources for");
		}else{
			//where content is generated to.
			def templates = new File(project.projectDir.absolutePath, "src/generated/resources/META-INF/resources/base-templates/screens")
			def beanDir = new File(project.projectDir.absolutePath, "src/beans/java")
			def beanPropsDir = new File(project.projectDir.absolutePath, "src/beans/resources")
			def java = new File(project.projectDir.absolutePath, "src/generated/java")
			def destSchemas= new File(project.projectDir.absolutePath,"src/generated/resources/schemas")
			
			//delete generated resources if they exist
			if(java.exists()){
				FileUtils.deleteDirectory(java);
			}
			if(beanDir.exists()){
				FileUtils.deleteDirectory(beanDir);
			}
			if(templates.exists()){
				FileUtils.deleteDirectory(templates);
			}
			if(destSchemas.exists()){
				FileUtils.deleteDirectory(destSchemas);
			}
			
			//copy schemas to destination (including display docs if they are there)
			FileUtils.copyDirectory(new File(source,"schemas"), destSchemas)			
			
			//where customized classes are defined (like customized Base*.java)
			def srcControlDir = new File(project.projectDir.absolutePath, "src/main/java")
			
			//generate the resources in the destination
			GenerateResources generator = new GenerateResources(project.name, new File(project.projectDir.absolutePath,"src/generated/resources").getAbsolutePath(), java.getAbsolutePath(), templates.getAbsolutePath(), srcControlDir.getAbsolutePath(), beanDir.getAbsolutePath(), beanPropsDir.getAbsolutePath());
			generator.process();
		}
    }
}
