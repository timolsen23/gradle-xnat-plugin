package org.nrg.xnat.plugins

import org.junit.Test
import org.gradle.testfixtures.ProjectBuilder
import org.gradle.api.Project
import org.nrg.xnat.tasks.XnatTask

import static org.junit.Assert.*

class XnatPluginTest {
    @Test
    public void xnatPluginAddsXnatTaskToProject() {
        Project project = ProjectBuilder.builder().build()
        project.pluginManager.apply XnatPlugin

        assertTrue(project.tasks.xnat instanceof XnatTask)
    }
}
