package org.nrg.xnat.tasks

import org.junit.Test
import org.gradle.testfixtures.ProjectBuilder
import org.gradle.api.Project
import org.nrg.xnat.tasks.XnatTask

import static org.junit.Assert.*

class XnatTaskTest {
    @Test
    public void canAddTaskToProject() {
        Project project = ProjectBuilder.builder().build()
        def task = project.task('xnat', type: XnatTask)
        assertTrue(task instanceof XnatTask)
    }
}
